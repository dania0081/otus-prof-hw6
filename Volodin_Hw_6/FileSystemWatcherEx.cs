﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Volodin_Hw_6
{
    public class FileSystemWatcherEx
    {
        public event EventHandler<FileArgs> FileFound;

        public void SearchFiles(string directory)
        {
            SearchFilesRecursive(directory);
        }

        private void SearchFilesRecursive(string directory)
        {
            if (FileFound == null)
                return;

            foreach (string filePath in Directory.GetFiles(directory))
            {
                var fileArgs = new FileArgs(filePath);
                FileFound.Invoke(this, fileArgs);

                // Check if cancellation is requested
                if (fileArgs.CancelSearch)
                    return;
            }

            foreach (string subDir in Directory.GetDirectories(directory))
            {
                SearchFilesRecursive(subDir);

                // Check if cancellation is requested
                if (FileFound != null && FileFound.GetInvocationList().Length == 0)
                    return;
            }
        }
    }
}
