﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Volodin_Hw_6
{
    public class FileArgs : EventArgs
    {
        public string FileName { get; }
        public bool CancelSearch { get; set; }

        public FileArgs(string fileName)
        {
            FileName = fileName;
        }
    }
}
