﻿namespace Volodin_Hw_6
{
    internal class Program
    {
        static void Main(string[] args)
        {
            // Test GetMax extension method
            List<int> numbers = new List<int> { 10, 5, 15, 8, 20 };
            int maxNumber = numbers.GetMax(x => x); // Find max element without any transformation
            Console.WriteLine("Max Number: " + maxNumber);

            // Test FileSearcher class
            FileSystemWatcherEx fileSearcher = new FileSystemWatcherEx();
            fileSearcher.FileFound += FileFoundHandler;
            fileSearcher.SearchFiles("C:\\Temp");
        }
        private static void FileFoundHandler(object sender, FileArgs e)
        {
            Console.WriteLine("File Found: " + e.FileName);

            // Cancel search if filename starts with "stop"
            if (e.FileName.StartsWith("stop"))
            {
                e.CancelSearch = true;
            }
        }
    }
}